﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;

namespace HomeworkBasic
{

class AppObj
{
//	public int Id {get; private set;} = 0;
	public string Name {get; private set;} = "";
	public string Description {get; private set;} = "";

public AppObj (string name = "", string desc = "")
{
//	Id = id;
	Name = name;
	Description = desc;
}
}


static class L
{
public static Object InvokeString (string str)
{
	str = str.Trim();
	if (String.IsNullOrEmpty(str)) return null;
	var obj = Type.GetType(str);
	if (obj == null)	return null;

	return Activator.CreateInstance(obj);
}

public static string Summ (string str, int len=20)
{
	if (str.Length < len)	return str;

	string cut = str.Substring(0, len-2);	// 3 ...

	int i = cut.LastIndexOf(' ');
	return cut.Substring(0, i) + "...";
}


public static void Show<T> (T[] ar)
{
	string com = "";

	foreach (var a in ar)
	{
		Console.Write(com + a);
		com = ", ";
	}
	Console.WriteLine();
}

public static bool IsUnique<T>(T[] ar, T val)
{
	for (int i = 0, len = ar.Length; i < len; ++i)
	{
		if (ar[i].Equals(val)) return false; 
	}

	return true;
}

public static T[] Reverse<T> (T[] ar)
{
	int len = ar.Length;
	int len1 = len - 1;
	T[] ret = new T[len];

	for (var i = 0; i < len; ++i)	ret[i] = ar[len1 - i];

	return ret;
}

/// <summary>
/// With dict
/// </summary>
public static bool Duplicates1 (int[] array)
{
	bool dup = false;
	var dictionary = new Dictionary<int, int>();
	foreach (var element in array)
	{
		//dictionary[element] = (dictionary.ContainsKey(element) ? dictionary[element] : 0) + 1;
		if (dictionary.ContainsKey(element))
		{
			dictionary[element] += 1;
			dup = true;
			// break;
		}
		else dictionary[element] = 1;
	}
//foreach (var pair in dictionary)
//{
//    Console.WriteLine(pair.Key + " repeats " + (pair.Value) + " times");
//}
	return dup;
}

/// <summary>
/// With linq
/// </summary>
/// <returns></returns>
public static bool Duplicates2 (int[] array)
{
	bool dup = false;

	foreach (var number in array.GroupBy(x => x))
	{
		//Console.WriteLine(number.Key + " repeats " + number.Count() + " times");
		if (number.Count() > 1) dup = true;
	}

return dup;
}


/// <summary>
/// O(n^2 /2)
/// </summary>
/// <param name="ar"></param>
/// <returns></returns>
public static bool Duplicates3 (int[] ar)
{
	bool dup = false;
	int len = ar.Length;

	for (int i = 0; i < len; ++i)
	{
		for (int j = i+1; j < len; ++j)
		{
			if (ar[i] == ar[j])
			{
				dup = true;
				break;
			}
		}
	}

	return dup;
}

/// <summary>
/// IsConsecutive 1,2,3 or 3,2,1
/// </summary>
/// <param name="ar">array</param>
/// <param name="direction">0 first found directions
/// 1 asc
/// -1 desc</param>
/// <returns>bool true if array is consecutive</returns>
public static bool IsConsecutive (int[] ar, int direction = 0) 
{
	int len = ar.Length;

	if (len < 2)	return false;			// auto false for len=0 or 1

	if (direction < 0)	direction = -1;
	else if (direction > 0)	direction = 1;
	else direction = ar[1] - ar[0];			// found direction, for asc 1

	for (int i = 1; i < len; ++i)		
	{
		int diff = ar[i] - ar[i-1];			// for asc 1. acs -1 -> ar[i-1] - ar[i]

		if ((diff != 1 && diff != -1) || direction != diff)		// Math.Abs(diff) != 1
		{
			return false;
		}
	}
return true;
}

/// <summary>
/// input string - InputString
/// </summary>
/// <param name="str"></param>
/// <returns></returns>
public static string PascalCase (string str)
{
	if (String.IsNullOrWhiteSpace(str))		return String.Empty;

	char[] a = str.ToCharArray();

	bool UP = true;		// true for first char
	int ws = 0;			// white space count
	int index = 0;		// current char index

	for (int i = 0; i < str.Length; ++i)
	{
		if (str[i] == ' ' || str[i] == '\t')
		{
			UP = true;
			++ws;
			continue;
		}
		if (UP)
		{
			a[index++] = char.ToUpper(a[i]);
			UP = false;
		}
		else
		{
			a[index++] = char.ToLower(a[i]);
		}
	}

	Array.Resize(ref a, str.Length - ws);	// maybe faster to count ws at first
	return new string(a);
}




//

/// <summary>
/// micro optimization
/// </summary>
/// <param name="str"></param>
/// <returns></returns>
public static string PascalCase2 (string str)
{
	if (String.IsNullOrWhiteSpace(str))		return String.Empty;

	bool UP = true;		// true for first char
	int index = 0;		// current char index
	char[] a = new char[str.Length - SumWhiteSpaces(str)];


	for (int i = 0, len = str.Length; i < len; ++i)
	{
		if (str[i] == ' ' || str[i] == '\t')
		{
			UP = true;
			continue;
		}
		if (UP)
		{
			a[index++] = char.ToUpper(a[i]);
			UP = false;
		}
		else
		{
			a[index++] = char.ToLower(a[i]);
		}
	}

	return new string(a);
}

/// <summary>
/// mosh solution
/// </summary>
public static string PascalCase4(string input)
{
	if (String.IsNullOrWhiteSpace(input))	return "";
	
	var variableName = "";
	foreach (var word in input.Split(' '))
	{
		var wordWithPascalCase = char.ToUpper(word[0]) + word.ToLower().Substring(1);
		variableName += wordWithPascalCase;
	}

	return variableName;
}

public static int SumWhiteSpaces (string s)
{
	int sum = 0;
	for (int i = 0, len = s.Length; i < len; ++i)
	{
		if (s[i] == ' ' || s[i] == '\t')	++sum;
	}
	return sum;
}

public static int CountVowels (string str)
{
	if (String.IsNullOrWhiteSpace(str))	return 0;
	int sum = 0;

	for (int i = 0, len = str.Length; i < len; ++i)
	{
		if (str[i] == 'a' || str[i] == 'e' || str[i] == 'i' || str[i] == 'o' || str[i] == 'u')	++sum;
	}
	return sum;
}

/// <summary>
/// return rnd string
/// </summary>
/// <param name="s">int</param>
/// <param name="n">1 kb, 2 mb, 3 gb</param>
/// <returns></returns>
public static string RndString (int s, int n)
{
	n = n >= 1 && n <= 3 ? n : 2;
	int size = s * 1024^n;

	char[] c = new char[size];
	Random r = new Random();

	for (int i = 0; i < size; ++i)
	{
		c[i] = (char) r.Next(32, 94);
	}

	return new string(c);
}


/// <summary>
/// count how many numbers between 1 and 100 are divisible by 3 with no remainder.
/// Display the count on the console.
/// </summary>
public static int SumDiv (int max, int div)
{
	int no = 0;

	for (int i = 0; i < max; ++i)
	{
		if (i % div == 0)	++no;
	}
	return no;
}



/// <summary>
/// count number of words in file
/// </summary>
/// <param name="path">string</param>
/// <param name="min">int min word length</param>
/// <returns>-1 not found or ulong count</returns>
public static long WordsInFile (string path)
{
	long count = 0;

	if (!File.Exists(path))	return -1;

	var lines = File.ReadLines(path);
	foreach (string line in lines)
	{
		var ar = line.Split(" ");

		count += ar.Length;
	}

return count;
}

/// <summary>
/// count number of words in file
/// </summary>
/// <param name="path">string</param>
/// <param name="min">int min word length</param>
/// <returns>-1 not found or ulong count</returns>
public static string LongestWordInFile (string path)
{
	if (!File.Exists(path))	return "";

	int max = 0;
	string maxWord = "";

	var lines = File.ReadLines(path);
	foreach (string line in lines)
	{
		string[] ar = line.Split(" ");

		foreach(string word in ar)
		{
			if (word.Length > max)
			{
				max = word.Length;
				maxWord = word;
			}
		}
	}

return maxWord;
}





public static double Factorial (int n)
{
	double res = 1;

	for (int i = 2; i <= n; ++i)
		res *= i;

	return res;
}

/// <summary>
/// works for big numbers
/// </summary>
/// <param name="n"></param>
/// <returns></returns>
public static BigInteger Factorial2 (int n)
{
	BigInteger res = new BigInteger(1);
	
	for (int i = 2; i <= n; ++i)
		res *= i;

	return res;
}


public static Func<int, double> Factorial3 = n => n == 0 ? 1 : 
    Enumerable.Range(1, n).Aggregate((acc, x) => acc * x);

/// <summary>
/// no good, 0 for 55555
/// </summary>
public static Func<int, BigInteger> Factorial6 = n => n == 0 ? 1 : 
    Enumerable.Range(1, n).Aggregate((acc, x) => acc * x);



/// <summary>
/// prone to stackoverflow exception :)
/// </summary>
/// <param name="n"></param>
/// <returns></returns>
public static BigInteger Factorial4 (int n)
{
	if (n <= 1) return new BigInteger(1);

	return n * Factorial4(n-1);
}



/*
int fac_times (int n, int acc = 1) {
    if (n == 0) return acc;
    else return fac_times(n - 1, acc * n);
}
int fac_times (int n, int acc) {
    if (n == 0) return acc;
    else return fac_times(n - 1, acc * n);
}

*/
/// <summary>
/// tail recursion failed
/// </summary>
/// <param name="n"></param>
/// <returns></returns>
public static BigInteger Factorial5 (int n, BigInteger res)
{
	if (n == 1) return res;
	else return Factorial5(n-1, res * n);
}










}}
