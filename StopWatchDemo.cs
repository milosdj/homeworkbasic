﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;
using System.Threading;

namespace HomeworkBasic
{
class StopWatchDemo
{
public StopWatchDemo ()
{
	StopWatch sw = new StopWatch();

	int[] ar = new [] {55, 5555, 99999};


	foreach (int i in ar)
	{
		sw.Start();
		BigInteger res = L.Factorial2(i);
		sw.Stop();

		Console.WriteLine(String.Format("Factorial {0} elapsed time: {1}ms  ", i, sw.Duration().TotalMilliseconds));
	}

	foreach (int i in ar)
	{
		sw.Start();
		Thread.Sleep(1000);
		sw.Stop();

		Console.WriteLine("Factorial {0} elapsed time: {1}ms  ", i, sw.Duration().TotalMilliseconds);
	}


	Console.ReadKey();



}



}}
