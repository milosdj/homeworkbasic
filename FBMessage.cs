﻿using System;
using System.Collections.Generic;

namespace HomeworkBasic
{
	class FBMessage
{
	private List<string> listPeople = new List<string>();
	private string line;
/// <summary>
/// - When you post a message on Facebook, depending on the number of people who like your post, Facebook displays different information.
///If no one likes your post, it doesn't display anything.
///If only one person likes your post, it displays: [Friend's Name] likes your post.
///If two people like your post, it displays: [Friend 1] and [Friend 2] like your post.
///If more than two people like your post, it displays: [Friend 1], [Friend 2] and [Number of Other People] others like your post.
///Write a program and continuously ask the user to enter different names, until the user presses Enter (without supplying a name). Depending on the number of names provided, display a message based on the above pattern.
/// </summary>
public FBMessage ()
{
while(true)
{
	Console.WriteLine("Write name + enter key, no name - end");
	
	while (true)
	{
		line = Console.ReadLine();
		if (String.IsNullOrWhiteSpace(line) || line == "q")		break;
		listPeople.Add(line);
	}
	if (line == "q")	break;

	
	string msg;
	int len = listPeople.Count;

	if (len > 2)
	{
		msg = String.Format("{0}, {1} and {2} others like your post.", listPeople[0], listPeople[1], len -2);
	}
	else if (len == 2)
	{
		msg = String.Format("{0} and {1} like your post.", listPeople[0], listPeople[1]);
	}
	else if (len == 1)
	{
		msg = listPeople[0] + "likes your post.";
	}
	else continue;

	Console.WriteLine(msg);
	listPeople.Clear();
}
	listPeople.Clear();
}}
}
