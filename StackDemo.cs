﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeworkBasic
{
class StackDemo
{
public StackDemo ()
{
	var stack = new Stack();
	stack.Push(1);
	stack.Push(2);
	stack.Push(3);
	Console.WriteLine(stack.Pop());
	Console.WriteLine(stack.Pop());
	Console.WriteLine(stack.Pop());
	
	Console.ReadKey();
}
}}
