﻿using System;

namespace HomeworkBasic
{
	class MaxNum
{
/// <summary>
/// ask the user to enter a series of numbers separated by comma.
/// Find the maximum of the numbers and display it on the console.
/// For example, if the user enters “5, 3, 8, 1, 4", the program should display 8.
/// </summary>
public MaxNum ()
{
while(true)
{
	Console.WriteLine("Enter comma separated numbers");
	string line = Console.ReadLine();
	if (line == "q")	break;

	int max = int.MinValue;
	string[] numbers = line.Split(',');

	for (int i = 0; i < numbers.Length; ++i)
	{
		bool ok = int.TryParse(numbers[i], out int n);
		if (ok && max < n)	max = n;
	}



	Console.WriteLine("Maximum is: {0}", max);
}
}}



}
