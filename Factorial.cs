﻿using System;
using System.Numerics;

namespace HomeworkBasic
{
	class Factorial
{

/// <summary>
/// user to enter a number.
/// Compute the factorial of the number and print it on the console.
/// For example, if the user enters 5, the program should calculate 5 x 4 x 3 x 2 x 1 and display it as 5! = 120.
/// </summary>
public Factorial ()
{
while(true)
{
	Console.WriteLine("enter number");
	string line = Console.ReadLine();
	if (line == "q")	break;

	int.TryParse(line, out int n);

	//double fact = L.Factorial3(n);
	BigInteger fact = L.Factorial2(n);

	int digits = (int)Math.Floor(BigInteger.Log10(fact) + 1);

	Console.WriteLine("Factoriel of {0}, digits: {1}\r\n{2}: ", n , digits, fact);
}}
}

}
