﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace HomeworkBasic
{


/// <summary>
/// void Push(object obj)
/// object Pop()
/// void Clear()
/// </summary>
class Stack
{
	private ArrayList _l = new ArrayList();

public void Push (object obj)
{
	if (obj == null)
	{
		// throw new InvalidOperationException();
		return;
	}
	_l.Add(obj);
}

public object Pop ()
{
	//  throw new InvalidOperationException();
	int last = _l.Count -1;
	if (last < 0)	return null;

	var r = _l[last];
	_l.RemoveAt(last);
	return r;
}

public void Clear ()
{
	_l.Clear();
}


}}
