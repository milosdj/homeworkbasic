﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeworkBasic
{

/// <summary>
/// Design a workﬂow engine that takes a workﬂow object and runs it.
/// have one method Run() that takes a workflow
/// then iterates over each activity in workﬂow and runs it.
/// 
/// workﬂow is a series of steps or activities.
/// We want our workﬂows to be extensible, so we can create new activities without impacting the 
/// existing activities. 
/// </summary>
class WorkflowDemo
{
public WorkflowDemo ()
{
	var we = new WorkflowEngine();

	var wf = new Workflow();


	wf.Add(new Run());
	wf.Add(new Upload());
	wf.Add(new WebNotify());
	// we.Add(new )

	we.Run(wf);

	Console.ReadKey();
}}
}
