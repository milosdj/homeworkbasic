﻿using System;

namespace HomeworkBasic
{
	class Rnd
{
/// <summary> 
/// Write a program that picks a random number between 1 and 10. Give the user 4 chances to guess the number.
/// If the user guesses the number, display “You won"; otherwise, display “You lost".
/// (To make sure the program is behaving correctly, you can display the secret number on the console first.)
/// </summary>
	private Random rnd = new Random();
public Rnd ()
{
while(true)
{
	int secret = rnd.Next(1, 11);
//	Console.WriteLine(secret);
	int chance = 4;
	string msg = "You lost";
	Console.WriteLine("I have a number from 1 to 10.");
	
	while (chance > 0)
	{
		Console.WriteLine("You have {0} chance{1} left.", chance, (chance > 1 ? "s" : ""));
		string line = Console.ReadLine();
		int.TryParse(line, out int guess);
	
		if (guess == secret)
		{
			msg = "You won";
			Console.WriteLine("Yes, it is a number " + guess);
			break;
		}
		Console.WriteLine("No, it is not a " + guess);
		--chance;
	}

	Console.WriteLine(msg);

}}
}

}
