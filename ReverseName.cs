﻿using System;

namespace HomeworkBasic
{
class ReverseName
{
/// <summary>
/// ask the user to enter their name. Use an array to reverse the name and then store the result in a new string.
/// Display the reversed name on the console.
/// </summary>
public ReverseName ()
{
while(true)
{
	Console.WriteLine("Write a name");
	string line = Console.ReadLine();
	if (line == "q")	break;
	if (String.IsNullOrWhiteSpace(line))	continue;

	char[] name = line.ToCharArray();

	// Array.Reverse(name);
	for (int i = 0; i < line.Length; ++i)
		name[i] = line[line.Length-1 - i];

	Console.WriteLine(new string(name));
}
}}


}
