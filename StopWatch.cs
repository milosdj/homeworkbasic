﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeworkBasic
{


/// <summary>
/// job of this class is to simulate a stopwatch.
/// provide two methods: Start and Stop. We call the start method ﬁrst, and the stop method next. 
/// Then we ask the stopwatch about the duration between start and stop. Duration should be a 
/// value in TimeSpan. Display the duration on the console. 
/// We should also be able to use a stopwatch multiple times. So we may start and stop it and then 
/// start and stop it again. Make sure the duration value each time is calculated properly. 
/// We should not be able to start a stopwatch twice in a row (because that may overwrite the initial 
/// start time). So the class should throw an InvalidOperationException if its started twice.
/// </summary>
class StopWatch
{

	private bool _inUse = false;
	private DateTime _start;
	private DateTime _stop;

public void Start ()
{
//	if (_inUse)	
//	{
//		throw new InvalidOperationException();
//	}
  if (!_inUse)
  {
	_inUse = true;
	_start = DateTime.Now;
  }
}

public void Stop ()
{
	if (_inUse)
	{
		_inUse = false;
		_stop = DateTime.Now;
	}
}

public TimeSpan Duration ()
{
	if (_inUse)	Stop();

	return _stop - _start;
}






}}
