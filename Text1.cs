﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeworkBasic
{
	class Text1
{
	

/// <summary>
/// Write a program and ask the user to enter a few numbers separated by a hyphen.
/// Work out if the numbers are consecutive:
/// if the input is "5-6-7-8-9" or "20-19-18-17-16", display: "Consecutive"
/// otherwise, display "Not Consecutive".
/// consecutive numbers - follow each other in order, without gaps
/// </summary>
public Text1 () 
{
while(true)
{
	Console.WriteLine("Write a few numbers separated by a hyphen:");
	string line = Console.ReadLine();
	if (line == "q")	break;

	string[] res = line.Split('-');

	int len = res.Length;
	int[] int_ar = new int[len];
	for (var i = 0; i < len; ++i)
	{
		bool ok = int.TryParse(res[i], out int num);
		if (ok) int_ar[i] = num;
	}

	string msg = L.IsConsecutive(int_ar) ? "Consecutive." : "Not Consecutive.";
	Console.WriteLine(msg);

//	Console.WriteLine(L.SumDiv(100, 3));
}}}
}
