﻿using System;
using System.Collections.Generic;

namespace HomeworkBasic
{
	class UniqNum
{
/// <summary>
/// ask the user to enter 5 numbers.
/// If a number has been previously entered, display an error message and ask the user to re-try.
/// Once the user successfully enters 5 unique numbers, sort them and display the result on the console.
/// </summary>

	private readonly List<int> numbers = new List<int>();
	private const int maxNum = 5;
public UniqNum ()
{
while(true)
{
	Console.WriteLine("Enter a number");
	string line = Console.ReadLine();
	if (line == "q")	break;
	if (String.IsNullOrWhiteSpace(line))	continue;

	bool ok = int.TryParse(line, out int num);

	if (!ok || numbers.Contains(num))
	{
		Console.WriteLine("Error, try again!");
		continue;
	}
	numbers.Add(num);

	if (numbers.Count == maxNum)
	{
		numbers.Sort();
		Console.WriteLine(string.Join(", ", numbers));
		numbers.Clear();
	}
}}
}

}
