﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeworkBasic
{
class Text4
{
/// <summary>
/// Write a program and ask the user to enter a few words separated by a space.
/// Use the words to create a variable name with PascalCase. "number of students", display "NumberOfStudents".
/// Make sure that the program is not dependent on the input.
/// So, if the user types "NUMBER OF STUDENTS", the program should still display "NumberOfStudents". 
/// </summary>
public Text4 ()
{
Console.WriteLine("enter a few words separated by a space");
while (true)
{
	string line = Console.ReadLine();
	if (line == "q")	break;

	
	Console.WriteLine(L.PascalCase(line));

}}


}}
