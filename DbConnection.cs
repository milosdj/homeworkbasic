﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeworkBasic
{
abstract class DbConnection
{

	private string ConnectionString;
	private TimeSpan Timeout;


public DbConnection (string con)
{
	if (String.IsNullOrWhiteSpace(con))
	{
		throw new ArgumentNullException();
	}

	ConnectionString = con;
}

public abstract void Run (string s);

public abstract void Open ();
public abstract void Close ();
}

class SqlConnection : DbConnection
{

public SqlConnection (string c) : base(c)
{

}
public override void Run(string s)
{
	Console.WriteLine(s);
}

public override void Open ()
{
	Console.WriteLine("SQL connection open.");
}

public override void Close ()
{
	Console.WriteLine("SQL connection closed.");
}


}



class OracleConnection : DbConnection
{
public OracleConnection (string c) : base(c)
{

}

public override void Run(string s)
{
	Console.WriteLine(s);
}


public override void Open ()
{
	Console.WriteLine("Oracle connection open.");
}

public override void Close ()
{
	Console.WriteLine("Oracle connection closed.");
}


}




class DbCommand
{
	DbConnection _dbc;
public DbCommand (DbConnection dbc)
{
	if (dbc == null)	throw new ArgumentNullException();

	_dbc = dbc;
}

public void NewConnection (DbConnection dbc)
{
	if (dbc == null)	throw new ArgumentNullException();

	_dbc = dbc;
}

public void Execute (string command)
{
	
	_dbc.Open();
	_dbc.Run(command);
	_dbc.Close();
}

}


}
