﻿using System;
using System.Collections.Generic;

namespace HomeworkBasic
{
class Program
{
private static List<AppObj> apps = new List<AppObj>()
{
new AppObj("Control1", "if numbers"),
new AppObj("SpeedCamera", "if ..."),
new AppObj("SumUserInput", "sum user numbers"),
new AppObj("Factorial", "factoriel numbers"),
new AppObj("Rnd", "guess a number"),
new AppObj("MaxNum", "max number"),
new AppObj(),

new AppObj("FBMessage", "list fb likes"),
new AppObj("ReverseName", "list string"),
new AppObj("UniqNum", "list unique nums"),
new AppObj("DuplicateList", "list unique in list"),
new AppObj("SmallList", "list 3 smallest"),
new AppObj(),

new AppObj("Text1", "consecutive numbers"),
new AppObj("Text2", "duplicate numbers"),
new AppObj("Text3", "time from string"),
new AppObj("Text4", "PascalCase string"),
new AppObj("Text5", "count vowels string"),
new AppObj(),

new AppObj("SOanswer", "duplicate numbers"),
new AppObj(),

new AppObj("File1", "read file, num of words"),
new AppObj("File2", "read file, longest word"),
new AppObj(),

new AppObj("StopWatchDemo", "intermediate stopwatch"),
new AppObj("SopDemo", "intermediate so post"),
new AppObj("StackDemo", "intermediate stack"),
new AppObj("DbDemo", "intermediate OOP"),
new AppObj("WorkflowDemo", "intermediate Interface"),


};

private static string pressAny = "\r\nPress any key to restart.";
static void Main(string[] args) { while(true)
{
	Console.Clear();
	Console.WriteLine("Choose app:	q-quit");

	int rows = 2;

	for (int i = 0; i < apps.Count; ++i)
	{
		AppObj app = apps[i];
		if (String.IsNullOrWhiteSpace(app.Name))
			Console.WriteLine();
		else if (app.Name == "-")
			Console.WriteLine("------------------");
		else
		{
			if (rows <= 1)	Console.WriteLine("{0} - {1} {2}", i, app.Name, app.Description);
			else
			{
				if (i % rows == 0)
				{
					Console.WriteLine("{0} - {1} {2}", i, app.Name, app.Description);
				}
				else
				{
					Console.Write("{0} - {1} {2}           - ", i, app.Name, app.Description);
				}

			}
		}

	}

	string line = Console.ReadLine();
	if (line.ToLower() == "q")	break; // Environment.Exit(0);


	if (!int.TryParse(line, out int index))	continue;

	AppObj a = apps[index];		//	Find(x => x.Id == reqi);

	if (String.IsNullOrWhiteSpace(a.Name))
	{
		Console.WriteLine("Empty class name:" + pressAny);
		Console.ReadKey();
		continue;
	}

	Console.Clear();
	var obj = L.InvokeString("HomeworkBasic."+a.Name);
	if (obj == null)
	{
		Console.WriteLine("Not found ClassName: " + a.Name + pressAny);
		Console.ReadKey();
		continue;
	}

}}






}}
