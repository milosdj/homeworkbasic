﻿using System;
using System.Collections.Generic;

namespace HomeworkBasic
{
	class SmallList
{
/// <summary>
/// ask the user to supply a list of comma separated numbers (e.g 5, 1, 9, 2, 10).
/// If the list is empty or includes less than 5 numbers, display "Invalid List" and ask the user to re-try;
/// otherwise, display the 3 smallest numbers in the list. 
/// </summary>
public SmallList ()
{
while(true)
{
	Console.WriteLine("Write comma separated numbers");
	string line = Console.ReadLine();
	if (line == "q")	break;
	
	string[] strings = line.Split(',');
	List<int> list = new List<int>();

	foreach (string s in strings)
	{
		bool ok = int.TryParse(s, out int n);
		if (ok) list.Add(n);
	}


	if (list.Count < 5)
	{
		Console.WriteLine("Invalid list!");
		continue;
	}
	else
	{
		list.Sort();
		if (list.Count > 3)		list.RemoveRange(3, list.Count-3);
		Console.WriteLine(String.Join(", ", list));
	}


}}

}


}
