﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeworkBasic
{
class Text3
{
/// <summary>
/// Write a program and ask the user to enter a time value in the 24-hour time format (e.g. 19:00).
/// A valid time should be between 00:00 and 23:59. If the time is valid, display "Ok";
/// otherwise, display "Invalid Time". If the user doesn't provide any values, consider it as invalid time. 
/// </summary>
public Text3 ()
{
while (true)
{
	Console.WriteLine("Enter a time value in 24-hour time format (e.g. 19:00)");
	string line = Console.ReadLine();
	if (line == "q")	break;	

	bool ok = DateTime.TryParse(line, out DateTime dt);

	string msg = ok ? "Ok" : "Not valid time";
	Console.WriteLine(msg);
}}

}}
