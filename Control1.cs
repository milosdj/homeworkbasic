﻿using System;

namespace HomeworkBasic
{
class Control1
{
/// <summary>
/// Write a program and ask the user to enter a number. The number should be between 1 to 10.
/// If the user enters a valid number, display "Valid" on the console. Otherwise, display "Invalid". (This logic is used a lot in applications where values entered into input boxes need to be validated.)
/// </summary>
public  Control1 () {
while (true)
{
	Console.WriteLine("enter a number");
	string line = Console.ReadLine();
	if (line == "q") break;

	int.TryParse(line, out int num);

	string msg = (num >= 1 && num <= 10 ) ? "Valid" : "Invalid";

	Console.WriteLine(msg);

}}

/// <summary>
/// Write a program which takes two numbers from the console and displays the maximum of the two.
/// </summary>
/// <param name="a"></param>
/// <param name="b"></param>
public int Max (int a, int b)
{
	return a < b ? b : a;
}
/// <summary>
///  Write a program and ask the user to enter the width and height of an image.
///  Then tell if the image is landscape or portrait. 
/// </summary>
public string WH (int w, int h)
{
	return w > h ? "landscape" : "portrait";
}


}}
