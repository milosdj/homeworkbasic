﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeworkBasic
{
class SoPost
{
	private string _title;
	private string _description;
	private DateTime _created;
	public int Vote {get; private set;} = 0;

/// <summary>
/// Design a class called Post. This class models a StackOverﬂow post.
/// properties for title, description and the date/time it was created.
/// able to up-vote or down-vote a post.
/// We should also be able to see the current vote value. In the main method, create a post, 
/// up-vote and down-vote it a few times and then display the current vote value. 
/// 
/// learn that a StackOverﬂow post should provide methods for up-voting and down-voting.
/// You should not give the ability to set the Vote property from the outside, 
/// because otherwise, you may accidentally change the votes of a class to 0 or to a random 
/// number. And this is how we create bugs in our programs. The class should always protect its 
/// state and hide its implementation detail
/// </summary>
public SoPost (string t, string d, DateTime c)
{
	_title = t;
	_description = d;
	_created = c;
}

public void UpVote ()
{
	++Vote;
}
public void DownVote ()
{
	--Vote;
}



}}
