﻿using System;

namespace HomeworkBasic
{
class SpeedCamera
{
/// <summary>
/// Write a program that asks the user to enter the speed limit. Once set, the program asks for the speed of a car. 
/// If the user enters a value less than the speed limit, program should display Ok on the console.
/// If the value is above the speed limit, the program should calculate the number of demerit points.
/// For every 5km/hr above the speed limit, 1 demerit points should be incurred and displayed on the console.
/// If the number of demerit points is above 12, the program should display License Suspended.
/// </summary>
public SpeedCamera ()
{
while (true)
{
	bool ok = true;
	int speedLimit = 0;
	int carSpeed = 0;

	Console.Write("enter a speed limit: ");
	string line = Console.ReadLine();
	if (line == "q") break;

	ok = int.TryParse(line, out speedLimit); 
	if (!ok || speedLimit < 10)
	{
		Console.WriteLine("\r\nMust be higher than 10.");
		continue;
	}

	Console.Write("Enter car speed: "); 
	line = Console.ReadLine();

	ok = int.TryParse(line, out carSpeed);
	if (!ok) continue;


	if (carSpeed <= speedLimit)
	{
		Console.WriteLine("Ok");
		continue;
	}
	else
	{
		int diff = carSpeed - speedLimit;
		int points = diff / 5;

		string msg;
		if (points > 12)	msg = "License Suspended";
		else msg = points + " demerit points";

		Console.WriteLine(msg);
	}
}}

}}
