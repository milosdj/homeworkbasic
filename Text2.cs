﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeworkBasic
{


class Text2
{

/// <summary>
/// Write a program and ask the user to enter a few numbers separated by a hyphen.
/// If the user simply presses Enter, without supplying an input, exit immediately;
/// otherwise, check to see if there are duplicates. If so, display "Duplicate" on the console. 
/// </summary>
public Text2 ()
{
while (true)
{
	Console.WriteLine("Write a few numbers separated by a hyphen:");
	string line = Console.ReadLine();
	if (line == "q" || String.IsNullOrEmpty(line))	break;

	string[] res = line.Split('-');
	int len = res.Length;

	int[] int_ar = new int[len];
	for (var i = 0; i < len; ++i)
	{
		bool ok = int.TryParse(res[i], out int num);
		if (ok)	int_ar[i] = num;
	}


	bool duplicate = L.Duplicates1(int_ar);

	string msg = duplicate ? "Duplicate" : "No duplicates";
	Console.WriteLine(msg);
}



}








}}
