﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HomeworkBasic
{
	class DuplicateList
{
/// <summary>
/// ask the user to continuously enter a number or type "Quit" to exit. The list of numbers may include duplicates.
/// Display the unique numbers that the user has entered.
/// </summary>

	private readonly List<int> list = new List<int>();

public DuplicateList ()
{
while(true)
{
	Console.WriteLine("Enter a number");
	string line = Console.ReadLine();
	if (line == "q")	break;

	if (String.IsNullOrWhiteSpace(line))
	{
		if (list.Count == 0)	
		{
			Console.WriteLine("Empty list.");
			continue;
		}

		// list = list.Distinct().ToList();
		List<int> l = new List<int>();
		// l.Add(list[0]); pa for (int i = 1;
		foreach (var n in list)	if (!l.Contains(n))	l.Add(n);
		
		Console.WriteLine(String.Join(", ", l));	
		list.Clear();
		continue;
	}

	bool ok = int.TryParse(line, out int num);
	if (!ok)	continue;
	list.Add(num);
}}
}


}
