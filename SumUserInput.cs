﻿using System;

namespace HomeworkBasic
{
	class SumUserInput
{
/// <summary>
/// continuously ask the user to enter a number or "ok" to exit.
/// Calculate the sum of all the previously entered numbers and display it on the console.
/// </summary>
public SumUserInput ()
{
	int sum = 0;
while(true)
{
	Console.WriteLine("enter number");
	string line = Console.ReadLine();
	if (line == "q")	break;
	if (line == "")
	{
		Console.WriteLine("Sum: " + sum);
		sum = 0;
	}
	else
	{
		int.TryParse(line, out int n);	// 0 on error
		sum += n;
	}
}
}}


}
