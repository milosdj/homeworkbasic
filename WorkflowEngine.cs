﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeworkBasic
{

interface IActivity
{
	void Execute();
}

interface IActivities
{
	void Add(IActivity a);
	void Remove(IActivity a);

	IEnumerable<IActivity> GetActivities ();
}


class Run : IActivity
{
	private string _activity = "Run";

	public void Execute ()
	{
		Console.WriteLine(_activity);
	}
}
class Upload : IActivity
{
	private string _activity = "Upload";

	public void Execute ()
	{
		Console.WriteLine(_activity);
	}
}

class WebNotify : IActivity
{
	private string _activity = "Your web servise is ready";

	public void Execute ()
	{
		Console.WriteLine(_activity);
	}
}



class Workflow : IActivities
{
	private readonly IList<IActivity> _a = new List<IActivity>();

	public void Add(IActivity a)
	{
		_a.Add(a);
	}
	
	public void Remove (IActivity a)
	{
		_a.Remove(a);
	}

	/// <summary>
	/// just enumerate, no add, remove
	/// </summary>
	/// <returns></returns>
	public IEnumerable<IActivity> GetActivities ()
	{
		return _a;
	}



}

class WorkflowEngine
{
	


public void Run (IActivities activities)
{
	foreach (var a in activities.GetActivities())
	{
		a.Execute();
	}
}



}}
